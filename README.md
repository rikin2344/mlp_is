------------ 3rd April, 2016 ------------------------

CHANGES:

* Code now works for a dataset in CSV file format  (ANY DATASET IN CSV FORMAT) (Can support more file format if time permits)
* Classification works for any number of input attributes but does only binary classification for now. (will support multiple classification soon!)
* Multiple Hidden layers (Oh Yeah!)
* Splitting training/testing -> 2/3rd & 1/3rd

NEXT STEPS:

* Support Multiple Classification 
* Add a GUI Component

------------ 13th March, 2016 ------------------------

CHANGES:

* Improved the input to the MLP.py
* Improved import code so as to import multiple types of input
* Asking the user for MLP's structure vs hard coding it. (Currently, we have this -> network = MLP1. (2,2,1))
* Now we auto detect how many input nodes are needed and also how many output nodes are needed based on the dataset provided by the user.


NEXT STEPS:

* Use a larger dataset from UCI repo 
* Pass in the dataset from command line, auto detect number of input/output nodes should work with any type of dataset
* Supporting multiple classification and not just binomial classification
* Improving the output reporting, adding graphs.
* Supporting multiple hidden layer, right now we are supporting only one hidden layer with variable number of hidden nodes

------------ 6th March, 2016 -------------------------

Basic code for MLP is up and kicking! 

Heres the output when making the algo learn OR logical function.

OUTPUT:

Learning the OR logical function

* 0 [ 0.  0.] 0.00 (expected 0.00)
* 1 [ 1.  0.] 0.98 (expected 1.00)
* 2 [ 0.  1.] 0.98 (expected 1.00)
* 3 [ 1.  1.] 1.00 (expected 1.00)

NEXT STEPS:

* Improve the input to the MLP.py
* Improve import code so as to import multiple types of input
* Asking the user for MLP's structure vs hard coding it. (Currently, we have this -> network = MLP1. (2,2,1))
* Supporting multiple classification and not just binomial classification