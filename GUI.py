from Tkinter import *
from tkFileDialog import askopenfilename
from Structure import readCSVFile, getInputNodesFromDataSet, getOutputNodesFromDataSet
from MLP import MLP

class GUI(Frame):
    
    def __init__(self, master):
        Frame.__init__(self, master)
        self.grid()
        self.create_widgets()
    
    def create_widgets(self):
        self.filePicker = Label(self, text="Dataset File:")
        self.filePicker.grid(row = 0, column = 0, sticky = W)
        
        self.filePath = Text(self, width = 26, height = 1, wrap = WORD)
        self.filePath.grid(row = 0, column = 1, columnspan = 2, sticky = W)
        self.filePath.config(highlightthickness=1)
        self.filePath.config(highlightbackground="gray")
        
        self.fileBrowse = Button(self, text="Browse", command = self.openFilePicker)
        self.fileBrowse.grid(row = 0, column = 2, sticky = W)
        
        self.numMiddleLayersLabel = Label(self, text="Number of Middle Layers:")
        self.numMiddleLayersLabel.grid(row = 2, column = 0, sticky = W)
        
        self.numMiddleLayers = Entry(self)
        self.numMiddleLayers.grid(row = 2, column = 1, sticky = W)
        
        self.numNodesMiddleLayersLabel = Label(self, text="Number of Nodes in Each Middle Layers:")
        self.numNodesMiddleLayersLabel.grid(row = 4, column = 0, sticky = W)
        
        self.numNodesMiddleLayers = Entry(self)
        self.numNodesMiddleLayers.grid(row = 4, column = 1, sticky = W)
        
        self.numEpochsLabel = Label(self, text="Number of Epochs: (Default: 2500)")
        self.numEpochsLabel.grid(row = 6, column = 0, sticky = W)
        
        self.numEpochs = Entry(self)
        self.numEpochs.grid(row = 6, column = 1, sticky = W)
        
        self.EmptyLabel = Label(self, text="")
        self.EmptyLabel.grid(row = 8, column = 0, sticky = W)
        
        self.runButton = Button(self, text="RUN", command = self.run)
        self.runButton.grid(row = 10, column = 1, sticky = W)
        
        self.EmptyLabel = Label(self, text="")
        self.EmptyLabel.grid(row = 12, column = 0, sticky = W)
        
        self.outputLabel = Label(self, text="Output Console:")
        self.outputLabel.grid(row = 14, column = 0, sticky = W)
        
        self.outputBox = Text(self, width = 80, height = 30, wrap = WORD)
        self.outputBox.grid(row = 16, column = 0, columnspan = 2, sticky = W)
        self.outputBox.config(highlightthickness=1)
        self.outputBox.config(highlightbackground="gray")

    
    
    def openFilePicker(self):
        filename = askopenfilename() # show an "Open" dialog box and return the path to the selected file
        self.filePath.delete(0.0, END)
        self.filePath.insert(0.0, filename)
    
    def run(self):
        path = self.filePath.get("1.0", END)
        path = path.strip()
        numMiddle = self.numMiddleLayers.get()
        numNodes = self.numNodesMiddleLayers.get()
        numEpochs = self.numEpochs.get()
        
        if numEpochs == "":
            numEpochs = "2500"
            print 'Epochs', int(numEpochs)    
        else:
            print 'Epochs', int(numEpochs)    
        
        numberOfNodesInMiddleLayer = []
        for i in range(int(numMiddle)):
            numberOfNodesInMiddleLayer.append(int(numNodes))
        
        samples, dataSet = readCSVFile(path)
        network = MLP(getInputNodesFromDataSet(dataSet),numberOfNodesInMiddleLayer,1)
        network.reset()
        print 'INITIALIZING LEARNING'
        network.learn(samples, dataSet, int(numEpochs))
        
    def updateOutputConsole(self, outputString):
        self.outputBox.insert(END, outputString)
        self.outputBox.insert(END, '\n')
        
root = Tk()
root.title("Turbo MLP")
root.geometry("700x900")
global app
app = GUI(root)
root.mainloop()




