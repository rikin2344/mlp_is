from Tkinter import *
from tkFileDialog import askopenfilename
import math
from Canvas import Line
from Structure import readCSVFile, getInputNodesFromDataSet
from MLP import MLP

class GUI2(Frame):
    
    startX = 50
    startY = 50
    maxWidth = 1000
    maxHeight = 900
    inNode = []
    outNode = []
    MiddleLayers = []
    lines = []
    labels = []
    fileName = ""
    numMiddleNode = 5
    numMiddleLayer = 1
    numInputNodes = 0
    
    def __init__(self, parent):
        Frame.__init__(self, parent)   
        self.parent = parent        
        self.initUI()
        
    def reset(self, canvas):
        self.startX = 50
        self.startY = 50
        self.maxWidth = 1000
        self.maxHeight = 900
        for node in self.inNode:
            canvas.delete(node)
        self.inNode = []
        for node in self.outNode:
            canvas.delete(node)
        self.outNode = []
        for layer in self.MiddleLayers:
            for nodes in layer:
                canvas.delete(nodes)
        self.MiddleLayers = []
        for line in self.lines:
            canvas.delete(line)
        self.lines = []
        for lab in self.labels:
            canvas.delete(lab)
        self.labels = []
        
        
    def initUI(self):
        self.parent.title("TURBO MLP")        
        self.pack(fill=BOTH, expand=1)
        canvas = Canvas(self)
        #self.updateUI(canvas)
        self.addButtons(canvas)
        canvas.pack(fill=BOTH, expand=1)

        
    def updateUI(self, canvas):
        self.reset(canvas)
        self.createInputNodes(canvas)
        self.createOutputNodes(canvas)
        self.createMiddleLayerNodes(canvas)
        self.createLines(canvas)
            
    def createInputNodes(self, canvas):
        tempY = (self.maxHeight / self.numInputNodes) - 20 
        lab = Label(self, text = "Input Layer")
        lab.configure(width = 10, activebackground = "#33B5E5", relief = FLAT)
        lab = canvas.create_window(self.startX - 35, tempY - 40, anchor=NW, window=lab)
        self.labels.append(lab)

        for i in range(self.numInputNodes):
            circle = canvas.create_oval(self.startX, tempY, self.startX + 25, tempY+25, outline="red", fill="green", width=2)
            self.inNode.append(circle)
            lab = Label(self, text = "i" + str(i + 1))
            lab.configure(width = 2, activebackground = "#33B5E5", relief = FLAT)
            lab = canvas.create_window(self.startX, tempY + 35, anchor=NW, window=lab)
            self.labels.append(lab)
            tempY += 80

            
    def createOutputNodes(self, canvas):
        tempX = self.maxWidth - 75
        tempY = math.ceil(self.maxHeight / 3) - 50
        lab = Label(self, text = "Output Layer")
        lab.configure(width = 10, activebackground = "#33B5E5", relief = FLAT)
        lab = canvas.create_window(tempX - 35, tempY - 40, anchor=NW, window=lab)
        for i in range(2):
            circle = canvas.create_oval(tempX, tempY, tempX + 25, tempY+25, outline="red", fill="orange", width=2)
            self.outNode.append(circle)
            lab = Label(self, text = "o" + str(i + 1))
            lab.configure(width = 2, activebackground = "#33B5E5", relief = FLAT)
            lab = canvas.create_window(tempX, tempY + 35, anchor=NW, window=lab)
            self.labels.append(lab)
            lab1 = Label(self, text = "Class " + str(i))
            lab1.configure(width = 5, activebackground = "#33B5E5", relief = FLAT)
            lab1 = canvas.create_window(tempX - 10, tempY + 60, anchor=NW, window=lab1)
            self.labels.append(lab1)
            tempY += tempY
    
    def createMiddleLayerNodes(self, canvas):

        tempX = self.maxWidth / (self.numMiddleLayer + 1) 
        tempY = (self.maxHeight / (self.numMiddleNode + 1)) - 20
        nodesList = [] 
        for j in range(1,self.numMiddleLayer + 1):
            tempXX = tempX*j
            tempY = (self.maxHeight / (self.numMiddleNode + 1)) - 20 
            nodesList = []
            lab = Label(self, text = "Hidden Layer " + str(j))
            lab.configure(width = 10, activebackground = "#33B5E5", relief = FLAT)
            lab = canvas.create_window(tempXX - 35, tempY - 40, anchor=NW, window=lab)
            self.labels.append(lab)
            for i in range(self.numMiddleNode):
                circle = canvas.create_oval(tempXX, tempY, tempXX + 25, tempY+25, outline="red", width=2)
                nodesList.append(circle)
                lab = Label(self, text = "h" + str(i + 1))
                lab.configure(width = 2, activebackground = "#33B5E5", relief = FLAT)
                lab = canvas.create_window(tempXX, tempY + 35, anchor=NW, window=lab)
                self.labels.append(lab)
                tempY += 80
            self.MiddleLayers.append(nodesList)
    
    def createLines(self, canvas):
        if len(self.MiddleLayers) > 0:
            self.createConnections(canvas, self.inNode, self.MiddleLayers[0])
        if len(self.MiddleLayers) > 1:
            for i in range(0, len(self.MiddleLayers) - 1):
                self.createConnections(canvas, self.MiddleLayers[i], self.MiddleLayers[i+1])
        self.createConnections(canvas, self.MiddleLayers[-1], self.outNode)    
            
    def createConnections(self, canvas, list1, list2):
        for circle in list1:
            for nodes in list2:
                line = canvas.create_line(canvas.coords(circle)[2], canvas.coords(circle)[3], canvas.coords(nodes)[0],canvas.coords(nodes)[1])
                self.lines.append(line)
                
    def addButtons(self, canvas):
        button1 = Button(self, text = "Pick Dataset", command = lambda:  self.openFilePicker(canvas), anchor = W)
        button1.configure(width = 10, activebackground = "#33B5E5", relief = FLAT)
        button1_window = canvas.create_window(10, 10, anchor=NW, window=button1)
    
        button2 = Button(self, text = "Add H Layer", command = lambda: self.addMiddleLayer(canvas), anchor = W)
        button2.configure(width = 10, activebackground = "#33B5E5", relief = FLAT)
        button2_window = canvas.create_window(150, 10, anchor=NW, window=button2)

        button3 = Button(self, text = "Remove H Layer ", command = lambda: self.removeMiddleLayer(canvas), anchor = W)
        button3.configure(width = 15, activebackground = "#33B5E5", relief = FLAT)
        button3_window = canvas.create_window(300, 10, anchor=NW, window=button3)

        button4 = Button(self, text = "Add Node", command = lambda:  self.addMiddleNode(canvas), anchor = W)
        button4.configure(width = 10, activebackground = "#33B5E5", relief = FLAT)
        button4_window = canvas.create_window(500, 10, anchor=NW, window=button4)

        button5 = Button(self, text = "Remove Node", command = lambda:  self.removeMiddleNode(canvas), anchor = W)
        button5.configure(width = 10, activebackground = "#33B5E5", relief = FLAT)
        button5_window = canvas.create_window(650, 10, anchor=NW, window=button5)
        
        button5 = Button(self, text = "RUN", command = self.run, anchor = W)
        button5.configure(width = 10, activebackground = "#33B5E5", relief = FLAT)
        button5_window = canvas.create_window(800, 10, anchor=NW, window=button5)
        
    def openFilePicker(self, canvas):
        self.filename = askopenfilename() # show an "Open" dialog box and return the path to the selected file
        self.filename = self.filename.strip()
        print self.fileName
        
        samples, dataSet = readCSVFile(self.filename)
        self.numInputNodes = getInputNodesFromDataSet(dataSet)
        
        self.updateUI(canvas)
        
    def addMiddleLayer(self, canvas):
        self.numMiddleLayer += 1
        self.updateUI(canvas)
        
    def removeMiddleLayer(self, canvas):              
        self.numMiddleLayer -= 1
        self.updateUI(canvas)
        
    def addMiddleNode(self, canvas):              
        self.numMiddleNode += 1
        self.updateUI(canvas)
            
    def removeMiddleNode(self, canvas):              
        self.numMiddleNode -= 1   
        self.updateUI(canvas)
        
    def run(self):
        
        samples, dataSet = readCSVFile(self.filename)        
        numberOfNodesInMiddleLayer = []
        for i in range(int(self.numMiddleLayer)):
            numberOfNodesInMiddleLayer.append(int(self.numMiddleNode))
            
        network = MLP(getInputNodesFromDataSet(dataSet),numberOfNodesInMiddleLayer,2)
        network.reset()
        print 'INITIALIZING LEARNING'
        network.learn(samples, dataSet, 2500)

        
def main():
  
    root = Tk()
    ex = GUI2(root)
    root.geometry("1000x900")
    root.mainloop()
      
if __name__ == '__main__':
    main()  