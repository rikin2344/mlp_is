# get number of inputs, build input nodes using this  
import csv
import numpy as np

def readCSVFile(filepath):
#     cr = csv.reader(open("/Users/Rikin/Downloads/CardiotocographyDataSet_CSV_2.csv", "rb"))    
#     cr = csv.reader(open("/Users/Rikin/Downloads/glass.data.txt", "rb"))    
#    cr = csv.reader(open("/Users/Rikin/Downloads/breast1.txt", "rb"))    
    cr = csv.reader(open(filepath, "rb"))    
    cr2 = []
    for row in cr:
        row2 = filter(None, row)
        row3 = map(int, row2)
        if len(row3) == 0:
            continue
        cr2.append(row3)
        
    samples = np.zeros(len(cr2), dtype = [('input', int, len(cr2[0]) - 1), ('output', int, 1)])
    i = 0
    for row in cr2:
        samples[i] = row[0:-1], row[-1]
#         print samples[i]
        i=i+1
    print 'NOW RETURNING SAMPLES AND CR2'
    return samples, cr2
               
def getInputNodesFromDataSet(dataSet):
    return len(dataSet[0]) - 1

def getOutputNodesFromDataSet(dataSet):
    outputList = set()
    for i in range(len(dataSet)):
        outputList.add(dataSet[i][-1])
    return len(outputList)

def getOutputSet(dataSet):
    outputList = set()
    for i in range(len(dataSet)):
        outputList.add(dataSet[i][-1])
    return outputList

def getInputNodes(samples):
    #return len(samples[0][0:-1])
    return len(samples[0][0])

# get number of unique output decision, build number of output nodes using this
def getOutputNodes(samples):
    outputList = set()
    for i in range(samples.size):
        outputList.add(samples[i][-1])
    return len(outputList)

def getMiddleLayerNodes():
    return input('Number of nodes in middle layer?: ')