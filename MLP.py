import numpy as np
from Structure import getOutputSet

def sigmoid(x):
    return np.tanh(x)

def dsigmoid(x):
    return 1.0-x**2

class MLP:

    def __init__(self, *args):
#         self.shape = args
        size = 0
        self.shape = []
        self.shape.append(args[0])
        size += 1
        for data in args[1]:
            self.shape.append(data)
            size += 1
        self.shape.append(args[2])
        size += 1
        
        n = size
        print "Length of args: " + str(size)
        print "Shape: " + str(self.shape)
        # Build layers
        self.layers = []
        
        # Input layer
        self.layers.append(np.ones(self.shape[0]+1))
        
        # Hidden layer(s) + output layer
        #print 'SHAPE: ', self.shape;
        for i in range(1,n):
            self.layers.append(np.ones(self.shape[i]))

        # Build weights matrix
        self.weights = []
        for i in range(n-1):
            self.weights.append(np.zeros((self.layers[i].size,
                                         self.layers[i+1].size)))

        self.dw = [0,]*len(self.weights)
        
        print "Total Number of Layers: " + str(len(self.layers))

        self.reset()

    def reset(self):
        for i in range(len(self.weights)):
            Z = np.random.random((self.layers[i].size,self.layers[i+1].size))
            self.weights[i][...] = (2*Z-1)*0.25

    def propagate_forward(self, data):
        
        # Set input layer
        self.layers[0][0:-1] = data
        
        # Propagate from layer 0 to layer n-1 using sigmoid as activation function
        for i in range(1,len(self.shape)):
            self.layers[i][...] = sigmoid(np.dot(self.layers[i-1],self.weights[i-1]))

        #print 'F:    ', self.layers[-1]
        return self.layers[-1]


    def propagate_backward(self, target, lrate=0.1, momentum=0.1):

        deltas = []

        # Compute error on output layer
        error = target - self.layers[-1]
        #print 'B:    ', target - self.layers[-1]
        delta = error*dsigmoid(self.layers[-1])
        deltas.append(delta)

        # Compute error on hidden layers
        for i in range(len(self.shape)-2,0,-1):
            delta = np.dot(deltas[0],self.weights[i].T)*dsigmoid(self.layers[i])
            deltas.insert(0,delta)
            
        # Update weights
        for i in range(len(self.weights)):
            layer = np.atleast_2d(self.layers[i])
            delta = np.atleast_2d(deltas[i])
            dw = np.dot(layer.T,delta)
            self.weights[i] += lrate*dw + momentum*self.dw[i]
            self.dw[i] = dw

        return (error**2).sum()


    def learn(self,samples, dataSet, epochs, lrate=.1, momentum=0.1):
        crTrain = samples[0: (len(samples)*2)/3]
        crTest = samples[((len(samples)*2)/3): len(samples)]
                
        # Train 
        print "NOW TRAINING..."
        for i in range(epochs):
            for j in range(len(crTrain)):
                self.propagate_forward( crTrain['input'][j] )
                self.propagate_backward( crTrain['output'][j], lrate, momentum )
                
        print 'NOW TESTING...'
        # Test
        uniqueOutputSet = list(getOutputSet(dataSet))
        Matrix = [[0 for x in range(len(uniqueOutputSet) + 1)] for y in range(len(uniqueOutputSet) + 1)]
        for i in range(len(uniqueOutputSet) + 1):
            for j in range(len(uniqueOutputSet) + 1):
                Matrix[i][j] = 0
        
        for i in range(len(uniqueOutputSet)):
            Matrix[0][i+1] = uniqueOutputSet[i]
            Matrix[i+1][0] = uniqueOutputSet[i]

        roundedNumberList= []
        tempList= []
        for i in range(len(crTest)):
            o = self.propagate_forward( crTest['input'][i] )
            roundedNumberList.append(o[0])
            print "" + str(i) + " " + str(crTest['input'][i]) + "  " + "CALCULATED OUTPUT: " + str(o[0]) + " (expected) " + str(crTest['output'][i])
        
        for i in range(len(roundedNumberList)):
            tempList.append(int(round(roundedNumberList[i])))
        
        roundedNumberList = tempList
        error = 0.0
        for i in range(len(roundedNumberList)):
            if roundedNumberList[i] != crTest['output'][i]:
                error += 1
            Matrix[roundedNumberList[i] + 1][crTest['output'][i] + 1] = int(Matrix[roundedNumberList[i] + 1][crTest['output'][i] + 1]) + 1 
        
        error = (error / len(roundedNumberList))*100
        print "ERROR RATE: " + str(error) + "%"
        
        print ""
        print "Confusion Matrix"
        print ""
        
        Matrix[0][0] = ""
        strOpt = ""
        for i in range(len(uniqueOutputSet) + 1):
            for j in range(len(uniqueOutputSet) + 1):
                strOpt += str(Matrix[i][j]) + "    "
            print strOpt
            strOpt = ""        
        
        
            

#    samples = np.zeros(4, dtype=[('input',  float, 2), ('output', float, 1)])
#     print "Learning the OR logical function"
#     samples[0] = (0,0), 0
#     samples[1] = (1,0), 1
#     samples[2] = (0,1), 1
#     samples[3] = (1,1), 1
#     network = MLP(getInputNodes(samples),getMiddleLayerNodes(),getOutputNodes(samples))

#     samples, dataSet = readCSVFile()
#     print 'CREATING NETWORK'
#     numberOfMiddleLayer = input('Number of Middle layer?: ')
#     numberOfNodesInMiddleLayer = []
#     for i in range(numberOfMiddleLayer):
#         number = input('Number of nodes in middle layer?: ')
#         numberOfNodesInMiddleLayer.append(number)
#     network = MLP(getInputNodesFromDataSet(dataSet),numberOfNodesInMiddleLayer,1)
#     network.reset()
#     print 'INITIALIZING LEARNING'
#     learn(network, samples)
